package libswaybar

/* cSpell:disable */
const MIC_ON_ICON = ""
const MIC_MUTE_ICON = ""
const MIC_HEADPHONE = ""
const SPEAKER_MUTE_ICON = ""
const SPEAKER_LOW_ICON = ""
const SPEAKER_LOUD_ICON = ""

const BAT_000_ICON = ""
const BAT_025_ICON = ""
const BAT_050_ICON = ""
const BAT_075_ICON = ""
const BAT_100_ICON = ""
const BAT_CHARCGE_ICON = ""

const USER_ICON = ""
const WIFI_ICON = ""
const SCREEN_ICON = ""
const CALENDAR_ICON = ""
