package libswaybar

/* cSpell:disable */
import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
)

func RunCmd(cmdName string, cmdArgs []string) string {
	cmd := exec.Command(cmdName, cmdArgs...)
	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()
	if err != nil {
		log.Fatal(err, fmt.Errorf(errb.String()))
	}
	return outb.String()
}
