package libswaybar

/* cSpell:disable */

import (
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func Get_brightness() string {
	br_cur_path := "/sys/class/backlight/*/brightness"
	br_max_path := "/sys/class/backlight/*/max_brightness"

	files, err := filepath.Glob(br_cur_path)
	if err != nil {
		panic(err)
	}

	brightness_b, err := os.ReadFile(files[0])
	if err != nil {
		panic(err)
	}

	brightness_i, err := strconv.Atoi(strings.Trim(string(brightness_b), "\n"))
	if err != nil {
		panic(err)
	}

	files, err = filepath.Glob(br_max_path)
	if err != nil {
		panic(err)
	}

	brightmax_b, err := os.ReadFile(files[0])
	if err != nil {
		panic(err)
	}

	brightmax_i, err := strconv.Atoi(strings.Trim(string(brightmax_b), "\n"))
	if err != nil {
		panic(err)
	}

	brightness := (brightness_i * 100) / brightmax_i

	return strings.Trim(strconv.Itoa(brightness), "\n")
}
