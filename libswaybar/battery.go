package libswaybar

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

/* cSpell:disable */

func Battery_charging() bool {
	battery_path := "/sys/class/power_supply/BAT*/status"
	files, err := filepath.Glob(battery_path)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		status, err := os.ReadFile(file)
		if err != nil {
			fmt.Print(err)
		}
		if strings.Contains(strings.Trim(string(status), "\n "), "Charging") {
			return true
		}
	}
	return false
}

func Battery_perc() int {
	perc_i := 0
	battery_path := "/sys/class/power_supply/BAT*/capacity"
	files, err := filepath.Glob(battery_path)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		perc_b, err := os.ReadFile(file)
		if err != nil {
			fmt.Print(err)
		}
		perc_s := strings.Trim(string(perc_b), "\n ")
		i, _ := strconv.Atoi(perc_s)
		perc_i = perc_i + i
	}
	return perc_i / len(files)
}
