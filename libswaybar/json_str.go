package libswaybar

/* cSpell:disable */
type SwayBarItem struct {
	Name     string `json:"name"`
	Instance string `json:"instance,omitempty"`
	FullText string `json:"full_text"`
	Color    string `json:"color,omitempty"`
	BgColor  string `json:"background,omitempty"`
	MinWidth string `json:"min_width,omitempty"`
	Align    string `json:"align,omitempty"`
}

type SwayBar struct {
	Version int  `json:"version"`
	ClickEv bool `json:"click_events"`
	Items   []SwayBarItem
}

/*
echo   $name@$hostname \|  $wifi_name \|  $current_brightness% \| $sink_icon $current_volume% \| $source_icon $current_source_volume% \| CPU: $current      _cpu_rounded% \| MEM: $current_mem_rounded% \| ↑ $uptime_formatted \| $battery_icon $battery_level_rounded% \|  $date_formatted

echo '{ "version": 1 }'
echo '['
echo '[]'
while :;
do
  echo ",[{\"name\":\"id_time\",\"full_text\":\"$(date)\",\"background\":\"#FF0000\"}]"
  sleep 1
done
*/
