/* Program to print the SSID of the current network */
/* getssid.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/wireless.h>

#define IW_INTERFACE "wlp4s0"

char *getssid(char *intname)
{
  struct iwreq wreq;
	memset(&wreq, 0, sizeof(struct iwreq));
	sprintf(wreq.ifr_name, intname);

	int sockfd;
	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		fprintf(stderr, "Cannot open socket \n");
		fprintf(stderr, "errno = %d \n", errno);
		fprintf(stderr, "Error description is : %s\n",strerror(errno));
		exit(1);
	}
	printf("Socket opened successfully \n");

	char *id = malloc(IW_ESSID_MAX_SIZE+1);
	wreq.u.essid.pointer = id;
	wreq.u.essid.length = IW_ESSID_MAX_SIZE;
	if (ioctl(sockfd, SIOCGIWESSID, &wreq)) {
		fprintf(stderr, "Get ESSID ioctl failed \n");
		fprintf(stderr, "errno = %d \n", errno);
		fprintf(stderr, "Error description : %s\n",strerror(errno));
		exit(2);
	}
	printf("IOCTL Successfull\n");
  return (char*)(wreq.u.essid.pointer);
}

// int main (void)
// {
//   char *a;
//   a = getssid(IW_INTERFACE);
// 	printf("ESSID is %s\n", a);
// 	exit(0);
// }

