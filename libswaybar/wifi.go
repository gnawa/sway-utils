package libswaybar

// #cgo CFLAGS: -g -Wall
// #include "getssid.h"
import "C"
import (
	"strings"
)

/* cSpell:disable */

// func Get_wifi_essid() string {
// 	w := "wlp4s0"
// 	return C.getssid(unsafe.StringData(w))
// }

// func Get_wifi_essid() string {
// 	sout := RunCmd("/sbin/iwgetid", []string{}) // wlp4s0    ESSID:"CurlyBracket"
// 	return strings.Trim(strings.Split(strings.Fields(sout)[1], ":")[1], "\"")
// }
//
// func Get_wifi_ifname() string {
// 	sout := RunCmd("/sbin/iwgetid", []string{})
// 	return strings.Fields(sout)[0]
// }

func Get_wifi_data() map[string]string {
	sout := RunCmd("/sbin/iwgetid", []string{})
	ifname := strings.Fields(sout)[0]
	essid := strings.Fields(sout)[1]
	essid = strings.Split(essid, ":")[1]
	essid = strings.Trim(essid, "\"")
	return map[string]string{
		"ESSID":  essid,
		"IFNAME": ifname,
	}
}

func Get_wifi_quality(wifi_ifname string) string {
	signal_quality := ""
	sout := RunCmd("/sbin/iwconfig", []string{wifi_ifname})
	fields_slc := strings.Split(sout, "\n")
	for _, v1 := range fields_slc {
		if strings.Contains(string(v1), "Quality") { // Link Quality=65/70  Signal level=-45 dBm
			quality_slc := strings.Fields(v1)
			for _, v2 := range quality_slc {
				if strings.Contains(string(v2), "Quality=") {
					signal_quality = strings.Split(v2, "=")[1]
				}
			}
		}
	}
	return signal_quality
}
