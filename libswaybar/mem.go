package libswaybar

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

/* cSpell:disable */

/*
/proc/meminfo

"MemTotal:"
"MemFree:"
"Buffers:"
"Cached:"
		u=t-(f+b+c); print u*100/t
*/

func Mem_used() (int, error) {
	memfile := "/proc/meminfo"
	memtotal := 0
	memfree := 0
	buffers := 0
	cached := 0
	memfile_b, err := os.ReadFile(memfile)
	if err != nil {
		fmt.Print(err)
	}
	memfile_s := string(memfile_b)
	memfile_l := strings.Split(memfile_s, "\n")

	for _, line := range memfile_l {
		if strings.HasPrefix(line, "MemTotal:") {
			memtotal, err = strconv.Atoi(strings.Fields(line)[1])
			// fmt.Println("t: " + strconv.Itoa(memtotal))
		}
		if strings.HasPrefix(line, "MemFree:") {
			memfree, err = strconv.Atoi(strings.Fields(line)[1])
			// fmt.Println("f: " + strconv.Itoa(memfree))
		}
		if strings.HasPrefix(line, "Buffers:") {
			buffers, err = strconv.Atoi(strings.Fields(line)[1])
			// fmt.Println("b: " + strconv.Itoa(buffers))
		}
		if strings.HasPrefix(line, "Cached:") {
			cached, err = strconv.Atoi(strings.Fields(line)[1])
			// fmt.Println("c: " + strconv.Itoa(cached))
		}
	}
	//fmt.Println(memfree + buffers + cached)
	return (memtotal - (memfree + buffers + cached)) * 100 / memtotal, err

}
