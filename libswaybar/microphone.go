package libswaybar

import (
	"encoding/json"
	"fmt"
)

/* cSpell:disable */

type Generic map[string]interface{}

type Mic struct {
	Index      int               `json:"index"`
	State      string            `json:"state"`
	Name       string            `json:"name"`
	Desc       string            `json:"description"`
	Driver     string            `json:"driver"`
	Mute       bool              `json:"mute"`
	Volume     Generic           `json:"volume"`
	Balance    float32           `json:"balance"`
	MonSrc     string            `json:"monitor_source"`
	Flags      []string          `json:"flags"`
	Properties map[string]string `json:"properties"`
	Ports      []Generic         `json:"ports"`
	ActPort    string            `json:"active_port"`
	Formats    []string          `json:"formats"`
}

type Mics []Mic

var mics Mics

func Get_mic_data() (Mics, error) {
	mic_data := RunCmd("/opt/pulseaudio/bin/pactl", []string{"-f", "json", "list", "sources"})
	err := json.Unmarshal([]byte(mic_data), &mics)
	if err != nil {
		fmt.Print(err)
	}
	return mics, err
}

func Get_mic_mute(mic_data Mics) bool {
	// RUNNING && active_port != null
	for _, source := range mic_data {
		if source.State == "RUNNING" {
			if source.ActPort != "" {
				if source.Mute == false {
					return false
				}
			}
		}
	}
	return true
}

func Get_active_port_type(active_port string, ports []Generic) string {
	port_type := ""
	for _, port := range ports {
		if port["name"] == active_port {
			port_type = port["type"].(string)
		}
	}
	return port_type
}
