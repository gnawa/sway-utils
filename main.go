package main

/* cSpell:disable */
import (
	"encoding/json"
	"fmt"
	"swaybar/libswaybar"
	"time"
)

func main() {
	barItem := libswaybar.SwayBarItem{
		Name:     "test",
		Instance: "inst",
		FullText: "BLAH",
	}

	bar := libswaybar.SwayBar{
		Version: 1,
		ClickEv: false,
		Items:   []libswaybar.SwayBarItem{barItem},
	}
	res, _ := json.Marshal(bar)
	//fmt.Println(libswaybar.MIC_ON_ICON)
	fmt.Println(string(res))

	time_now := time.Now()

	wifi_data := libswaybar.Get_wifi_data()
	wifi_essid := wifi_data["ESSID"]
	wifi_ifname := wifi_data["IFNAME"]
	wifi_quality := libswaybar.Get_wifi_quality(wifi_ifname)

	fmt.Printf("WIFI: %s, %s, %s\n", wifi_ifname, wifi_essid, wifi_quality)
	fmt.Printf("DISP: %s\n", libswaybar.Get_brightness())
	// Volume
	mic_data, err := libswaybar.Get_mic_data()
	fmt.Printf("MIC MUTE: %t\n", libswaybar.Get_mic_mute(mic_data))
	if err != nil {
		fmt.Println(err)
	}
	// CPU
	mem, err := libswaybar.Mem_used()
	fmt.Printf("MEM: %d%%\n", mem)
	// Uptime
	fmt.Printf("BAT CHG: %t\n", libswaybar.Battery_charging())
	fmt.Printf("BAT PRC: %d%%\n", libswaybar.Battery_perc())
	fmt.Printf("%s\n", time_now.Format("Wed 2006-01-02 15:04"))
	//for _, m := range mic_data {
	//	fmt.Printf("%d %s\n%s\n", m.Index, libswaybar.Get_active_port_type(m.ActPort, m.Ports), m.Volume)
	//}
}
